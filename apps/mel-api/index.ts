import { program } from "commander";
import { start } from "./src";

program
  .command("api")
  .description("Start the API server")
  .action(async () => {
    start();
  });
