import { defineConfig } from "vite";
import { VitePluginNode } from "vite-plugin-node";
import tsconfigPaths from "vite-tsconfig-paths";

export default defineConfig({
  // ...vite configures
  server: {
    // vite server configs, for details see [vite doc](https://vitejs.dev/config/#server-host)
    port: 3001,
  },
  plugins: [
    tsconfigPaths(),
    ...VitePluginNode({
      adapter: "koa",
      appPath: "./main.ts",
      exportName: "mel-api",
      tsCompiler: "esbuild",
      swcOptions: {},
    }),
  ],
});
