export type CollectionStats = {
  ns: string;
  host: string;
  localTime: Date;
  storageStats: {
    size: number;
    count: number;
    avgObjSize: number;
    storageSize: number;
    freeStorageSize: number;
    capped: boolean;
    wiredTiger: any;
    nindexes: number;
    indexDetails: any;
    indexBuilds: string[];
    totalIndexSize: number;
    totalSize: number;
    indexSizes: {
      [index: string]: number;
    };
    scaleFactor: number;
  };
};
