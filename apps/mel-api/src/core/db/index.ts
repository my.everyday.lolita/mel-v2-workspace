import mongoose from "mongoose";

export * from "./types";

export async function connect() {
  await mongoose.connect(
    `mongodb://${process.env.MONGO_URL!}/${process.env.MONGO_DB!}`,
    {
      auth: {
        username: process.env.MONGO_ROOT!,
        password: process.env.MONGO_ROOT_PASSWORD!,
      },
      connectTimeoutMS: 2000,
      serverSelectionTimeoutMS: 2000,
    }
  );
  return mongoose.connection.db;
}
