import { beforeEach, describe, expect, it } from "vitest";
import {
  Delete,
  Get,
  Patch,
  Post,
  Prefix,
  Put,
  Router,
  getRoutes,
} from "./Router";

describe("Router", () => {
  const prefix = "/test";
  @Prefix(prefix)
  class TestRouter extends Router {
    @Get("/get")
    getHandler() {}

    @Post("/post")
    postHandler() {}

    @Put("/put")
    putHandler() {}

    @Patch("/patch")
    patchHandler() {}

    @Delete("/delete")
    deleteHandler() {}

    getKoaRouter() {
      return this.router;
    }
  }

  let router: TestRouter;

  beforeEach(() => {
    router = TestRouter.create();
  });

  it("should correctly add route data with decorators", () => {
    const data = getRoutes(router);
    expect(data).toEqual([
      { method: "get", path: "/get", handler: "getHandler", middleware: [] },
      { method: "post", path: "/post", handler: "postHandler", middleware: [] },
      { method: "put", path: "/put", handler: "putHandler", middleware: [] },
      {
        method: "patch",
        path: "/patch",
        handler: "patchHandler",
        middleware: [],
      },
      {
        method: "delete",
        path: "/delete",
        handler: "deleteHandler",
        middleware: [],
      },
    ]);
  });

  it("should correctly initialize routes with init", () => {
    expect(router.getKoaRouter().stack.map((route) => route.path)).toEqual([
      `${prefix}/get`,
      `${prefix}/post`,
      `${prefix}/put`,
      `${prefix}/patch`,
      `${prefix}/delete`,
    ]);
  });

  it("should correctly create a Router instance", () => {
    expect(router).toBeInstanceOf(TestRouter);
    expect(router.getKoaRouter().stack.map((route) => route.path)).toEqual([
      `${prefix}/get`,
      `${prefix}/post`,
      `${prefix}/put`,
      `${prefix}/patch`,
      `${prefix}/delete`,
    ]);
  });

  it("should correctly add prefix to the koa router", () => {
    expect(
      router
        .getKoaRouter()
        .stack.every((route) => route.path.startsWith(prefix))
    ).toBeTruthy();
  });
});
