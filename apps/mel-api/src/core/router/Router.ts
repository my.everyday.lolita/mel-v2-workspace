import { Middleware } from "koa";
import KoaRouter from "koa-router";
import { compose } from "../utils";

const _ROUTER_ROUTES_KEY = Symbol("mel-api:core:router:routes");
const _ROUTER_DATA_KEY = Symbol("mel-api:core:router:data");

export type RouteMethod = "get" | "post" | "put" | "patch" | "delete";

export type RouteData = {
  method: RouteMethod;
  path: string;
  handler: string;
  middleware?: Middleware[];
};

export type RouterData = {
  prefix: string;
};

export type ChildRouteData = { path: string; router: Router };

/**
 * @private
 * Export for testing purposes only
 */
export function getRoutes(target: InstanceType<typeof Router>) {
  if (!Reflect.has(target, _ROUTER_ROUTES_KEY)) {
    Reflect.defineProperty(target, _ROUTER_ROUTES_KEY, {
      value: [],
      enumerable: false,
      configurable: true,
    });
  }
  return Reflect.get(target, _ROUTER_ROUTES_KEY) as RouteData[];
}

/**
 * @private
 * Export for testing purposes only
 */
export function getRouterData(target: InstanceType<typeof Router>) {
  if (!Reflect.has(target, _ROUTER_DATA_KEY)) {
    Reflect.defineProperty(target, _ROUTER_DATA_KEY, {
      value: {},
      enumerable: false,
      configurable: true,
    });
  }
  return Reflect.get(target, _ROUTER_DATA_KEY) as RouterData;
}

function handleRouteData(
  target: InstanceType<typeof Router>,
  routeData: RouteData
) {
  const data = getRoutes(target);
  data.push(routeData);
}

export function Get(path?: string, middleware: Middleware[] = []) {
  return function (
    target: InstanceType<typeof Router>,
    key: string,
    _: PropertyDescriptor
  ) {
    handleRouteData(target, {
      method: "get",
      path: path ?? "/",
      handler: key,
      middleware,
    });
  };
}

export function Post(path?: string, middleware: Middleware[] = []) {
  return function (
    target: InstanceType<typeof Router>,
    key: string,
    _: PropertyDescriptor
  ) {
    handleRouteData(target, {
      method: "post",
      path: path ?? "/",
      handler: key,
      middleware,
    });
  };
}

export function Put(path?: string, middleware: Middleware[] = []) {
  return function (
    target: InstanceType<typeof Router>,
    key: string,
    _: PropertyDescriptor
  ) {
    handleRouteData(target, {
      method: "put",
      path: path ?? "/",
      handler: key,
      middleware,
    });
  };
}

export function Patch(path?: string, middleware: Middleware[] = []) {
  return function (
    target: InstanceType<typeof Router>,
    key: string,
    _: PropertyDescriptor
  ) {
    handleRouteData(target, {
      method: "patch",
      path: path ?? "/",
      handler: key,
      middleware,
    });
  };
}

export function Delete(path?: string, middleware: Middleware[] = []) {
  return function (
    target: InstanceType<typeof Router>,
    key: string,
    _: PropertyDescriptor
  ) {
    handleRouteData(target, {
      method: "delete",
      path: path ?? "/",
      handler: key,
      middleware,
    });
  };
}

export function Prefix(prefix: string) {
  return function (target: InstanceType<typeof Router>) {
    const data = getRouterData(target);
    data.prefix = prefix;
  };
}

export abstract class Router {
  protected router = new KoaRouter();
  protected children: ChildRouteData[] = [];

  constructor(_: never) {}

  routes() {
    return this.router.routes();
  }

  allowedMethods(options?: KoaRouter.IRouterAllowedMethodsOptions) {
    return this.router.allowedMethods(options);
  }

  protected init() {
    const routes = getRoutes(this);
    const data = getRouterData(this.constructor);
    if (data.prefix) {
      this.router.prefix(data.prefix);
    }
    routes.forEach(({ method, path, handler, middleware }) => {
      this.router[method](
        handler,
        path,
        compose(...(middleware ?? [])),
        async (ctx, next) => {
          const body = await (this[handler as keyof this] as Function)(
            ctx,
            next
          );
          ctx.body = typeof body === "string" ? body : JSON.stringify(body);
        }
      );
    });
    this.children.forEach(({ path, router }) => {
      this.router.use(path, router.routes(), router.allowedMethods());
    });
    return this;
  }

  static create<T extends Router>(this: new (internalGuard: never) => T) {
    return new this(undefined as never).init();
  }
}
