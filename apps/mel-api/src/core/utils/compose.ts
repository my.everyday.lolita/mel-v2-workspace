import { Context, Middleware, Next } from "koa";

/**
 * Inspired by koa-compose
 */
export function compose(...args: Middleware[]) {
  return async (ctx: Context, next: Next) => {
    let index = -1;
    async function dispatch(i: number): Promise<void> {
      if (i <= index) {
        throw new Error("next() called multiple times");
      }
      index = i;
      const fn = i === args.length ? next : args[i];
      if (!fn) {
        return;
      }
      return fn(ctx, dispatch.bind(null, i + 1));
    }
    return dispatch(0);
  };
}
