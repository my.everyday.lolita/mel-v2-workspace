import { Context, Next } from "koa";
import { describe, expect, it } from "vitest";
import { compose } from "./compose";

describe("compose", () => {
  it("should return a function", () => {
    const middleware = compose();
    expect(typeof middleware).toBe("function");
  });

  it("should execute middleware in order", async () => {
    const order: number[] = [];
    const middleware1 = async (ctx: Context, next: Next) => {
      order.push(1);
      await next();
      order.push(4);
    };
    const middleware2 = async (ctx: Context, next: Next) => {
      order.push(2);
      await next();
      order.push(3);
    };
    const middleware = compose(middleware1, middleware2);
    await middleware({} as Context, async () => {});
    expect(order).toEqual([1, 2, 3, 4]);
  });

  it("should call next after all middleware", async () => {
    let nextCalled = false;
    const middleware = compose(async (ctx: Context, next: Next) => {
      await next();
      nextCalled = true;
    });
    await middleware({} as Context, async () => {});
    expect(nextCalled).toBe(true);
  });

  it("should throw error when next is called multiple times", async () => {
    const middleware = compose(async (ctx: Context, next: Next) => {
      await next();
      await next();
    });
    await expect(middleware({} as Context, async () => {})).rejects.toThrow(
      "next() called multiple times"
    );
  });
});
