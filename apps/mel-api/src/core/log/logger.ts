import mongoose from "mongoose";
import { config, createLogger, format, transports } from "winston";
import "winston-mongodb";

/**
 * logging levels are prioritized from 0 to 6 (highest to lowest):
 *
 * - emerg: 0,
 * - alert: 1,
 * - crit: 2,
 * - error: 3,
 * - warning: 4,
 * - notice: 5,
 * - info: 6,
 * - debug: 7,
 *
 */
export const logger = createLogger({
  levels: config.syslog.levels,
  transports: [
    new transports.Console({
      level: "debug",
      format: format.combine(
        format.prettyPrint(),
        format.colorize(),
        format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
        format.printf(({ timestamp, level, message }) => {
          return `${timestamp} ${level}: ${message}`;
        })
      ),
    }),
    // new transports.Console({
    //   level: "error",
    //   format: format.combine(
    //     format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
    //     format.json()
    //   ),
    // }),
  ],
});

export function registerMongoDBTransport(db: mongoose.mongo.Db) {
  logger.add(
    new transports.MongoDB({
      level: "notice",
      db: Promise.resolve(db),
      dbName: process.env.MONGO_DB!,
      collection: "logs",
      capped: true,
      storeHost: false,
      cappedSize: 1024 * 100,
      format: format.combine(format.timestamp(), format.json()),
    })
  );
}
