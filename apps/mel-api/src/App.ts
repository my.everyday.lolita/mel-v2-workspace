import cors from "@koa/cors";
import Koa from "koa";
import { Router, connect, logger, registerMongoDBTransport } from "./core";
import { ResourcesRouter } from "./features/resources";

export const app = new Koa();
const allowedOrigins = process.env.ALLOWED_ORIGINS?.split(" ") ?? ["localhost"];
const PORT = process.env.PORT ?? 3001;

export function registerRoutes(router: Router) {
  app.use(router.routes());
  app.use(router.allowedMethods());
}

export async function start() {
  app.on("error", (error) => {
    logger.error("Server error", error);
  });
  app.use(
    cors({
      origin(ctx) {
        if (
          ctx.request.header.origin &&
          allowedOrigins.some((pattern) =>
            ctx.request.header.origin?.includes(pattern)
          )
        ) {
          return ctx.request.header.origin;
        }
        return allowedOrigins[0];
      },
    })
  );
  logger.info("Registering routes");
  registerRoutes(ResourcesRouter.create());
  logger.info("Routes registered");
  logger.info("Connecting to database");
  try {
    const db = await connect();
    logger.info("Connected to database");
    registerMongoDBTransport(db);
  } catch (error) {
    logger.error("Failed to connect to database", error);
    logger.info("Exiting due to database connection error");
    return;
  }
  app.listen(PORT, () => {
    logger.debug(`Server started on port ${PORT}`);
    logger.debug(`Allowed origins: ${allowedOrigins}`);
  });
}
