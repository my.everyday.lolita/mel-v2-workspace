import { Context } from "koa";
import { koaBody } from "koa-body";
import { Delete, Get, Patch, Post, Put, Router } from "../../../core";
import { authenticate } from "../../authentication";
import { checkResourceId } from "../utils";
import { FeatureFacade } from "./Feature";

export class FeatureRouter extends Router {
  facade = new FeatureFacade();

  @Get()
  async findAll() {
    return await this.facade.findAll();
  }

  @Put("/", [koaBody(), authenticate(["admin", "create-feature"])])
  async create(ctx: Context) {
    const entity = ctx.request.body;
    return await this.facade.create(entity);
  }

  @Patch("/", [
    koaBody(),
    checkResourceId(),
    authenticate(["admin", "edit-feature"]),
  ])
  async update(ctx: Context) {
    const entity = ctx.request.body;
    return await this.facade.update(entity);
  }

  @Delete("/", [koaBody(), checkResourceId(), authenticate(["admin"])])
  async delete(ctx: Context) {
    const entity = ctx.request.body;
    return await this.facade.delete(entity);
  }

  @Post("/import", [koaBody(), authenticate(["admin"])])
  async insertMany(ctx: Context) {
    const entities = ctx.request.body;
    return await this.facade.insertMany(entities);
  }
}
