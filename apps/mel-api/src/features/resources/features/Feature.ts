import { Feature } from "mel-types";
import mongoose, { Document, Schema } from "mongoose";
import { CategorySchema } from "../categories";
import { BaseResourceFacade } from "../utils";

export type FeatureDocument = Feature & Document;

const FeatureSchema = new Schema<Feature>({
  name: { type: String, required: true },
  categories: { type: [CategorySchema], required: true },
});

export const FeatureModel = mongoose.model("Feature", FeatureSchema);

export class FeatureFacade extends BaseResourceFacade<
  Feature,
  typeof FeatureModel,
  FeatureDocument
> {
  model = FeatureModel;
}
