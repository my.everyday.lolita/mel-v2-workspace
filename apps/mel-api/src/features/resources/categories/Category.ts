import { Category } from "mel-types";
import mongoose, { Document, Schema } from "mongoose";
import { BaseResourceFacade } from "../utils";

export type CategoryDocument = Category & Document;

export const CategorySchema = new Schema<Category>();

CategorySchema.add({
  name: { type: String, required: true },
  shortname: { type: String, required: true },
  children: { type: [CategorySchema], required: true },
});

export const CategoryModel = mongoose.model("Category", CategorySchema);

export class CategoryFacade extends BaseResourceFacade<
  Category,
  typeof CategoryModel,
  CategoryDocument
> {
  model = CategoryModel;
}
