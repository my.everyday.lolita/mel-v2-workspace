import { ChildRouteData, Prefix, Router } from "../../core";
import { BrandRouter } from "./brands";
import { CategoryRouter } from "./categories";
import { ColorRouter } from "./colors";
import { FeatureRouter } from "./features";
import { UserContentRouter } from "./user-content";

@Prefix("/api/resources")
export class ResourcesRouter extends Router {
  protected children: ChildRouteData[] = [
    { path: "/brands", router: BrandRouter.create() },
    { path: "/colors", router: ColorRouter.create() },
    { path: "/features", router: FeatureRouter.create() },
    { path: "/categories", router: CategoryRouter.create() },
    { path: "/user-contents", router: UserContentRouter.create() },
  ];
}
