import { Document, Model } from "mongoose";
import { CollectionStats } from "../../../core";

export abstract class BaseResourceFacade<
  DTO,
  M extends Model<DTO>,
  D extends Document
> {
  abstract model: M;

  async getById(id: string) {
    return this.model.findById(id).exec();
  }

  async create(entity: DTO) {
    const instance = new this.model(entity);
    return instance.save();
  }

  async update(entity: D) {
    if (!(await this.model.findById(entity._id))) {
      throw new Error("Entity not found");
    }
    return this.model.findByIdAndUpdate(entity._id, entity as any);
  }

  async insertMany(entities: DTO[]) {
    return this.model.insertMany(entities);
  }

  async findAll() {
    return this.model.find().sort({ name: "asc" }).exec();
  }

  async delete(entity: D) {
    return this.model.findByIdAndDelete(entity._id).exec();
  }

  async totalSize() {
    const stats = await this.model.aggregate<CollectionStats>([
      { $collStats: { storageStats: {} } },
    ]);
    const storageStats = stats[0].storageStats;
    return storageStats.storageSize + storageStats.totalIndexSize;
  }
}
