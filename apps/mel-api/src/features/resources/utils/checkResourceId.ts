import { Middleware } from "koa";

export function checkResourceId(): Middleware {
  return async (ctx, next) => {
    const { _id } = ctx.request.body;
    if (!_id) {
      ctx.status = 400;
      return;
    }
    await next();
  };
}
