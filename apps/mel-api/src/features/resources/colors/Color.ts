import { Color } from "mel-types";
import mongoose, { Document, Schema } from "mongoose";
import { BaseResourceFacade } from "../utils";

export type ColorDocument = Color & Document;

const ColorSchema = new Schema<Color>({
  name: { type: String, required: true },
  hex: { type: String, required: true },
});

export const ColorModel = mongoose.model("Color", ColorSchema);

export class ColorFacade extends BaseResourceFacade<
  Color,
  typeof ColorModel,
  ColorDocument
> {
  model = ColorModel;
}
