import { Context } from "koa";
import { koaBody } from "koa-body";
import { Delete, Get, Patch, Post, Put, Router } from "../../../core";
import { authenticate } from "../../authentication";
import { checkResourceId } from "../utils";
import { BrandFacade } from "./Brand";

export class BrandRouter extends Router {
  facade = new BrandFacade();

  @Get()
  async findAll() {
    return await this.facade.findAll();
  }

  @Put("/", [koaBody(), authenticate(["admin", "create-brand"])])
  async create(ctx: Context) {
    const brand = ctx.request.body;
    return await this.facade.create(brand);
  }

  @Patch("/", [
    koaBody(),
    checkResourceId(),
    authenticate(["admin", "edit-brand"]),
  ])
  async update(ctx: Context) {
    const brand = ctx.request.body;
    return await this.facade.update(brand);
  }

  @Delete("/", [koaBody(), checkResourceId(), authenticate(["admin"])])
  async delete(ctx: Context) {
    const brand = ctx.request.body;
    return await this.facade.delete(brand);
  }

  @Post("/import", [koaBody(), authenticate(["admin"])])
  async insertMany(ctx: Context) {
    const brands = ctx.request.body;
    return await this.facade.insertMany(brands);
  }
}
