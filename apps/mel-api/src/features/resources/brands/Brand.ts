import { Brand } from "mel-types";
import mongoose, { Document, Schema } from "mongoose";
import { BaseResourceFacade } from "../utils";

export type BrandDocument = Brand & Document;

const BrandSchema = new Schema<Brand>({
  name: { type: String, required: true },
  shortname: { type: String, required: true },
  shop: { type: String, required: true },
});

export const BrandModel = mongoose.model("Brand", BrandSchema);

export class BrandFacade extends BaseResourceFacade<
  Brand,
  typeof BrandModel,
  BrandDocument
> {
  model = BrandModel;
}
