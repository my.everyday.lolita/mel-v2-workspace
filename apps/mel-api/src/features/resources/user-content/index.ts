import { Context } from "koa";
import { koaBody } from "koa-body";
import { Get, Patch, Router } from "../../../core";
import { authenticate } from "../../authentication";
import { checkResourceId } from "../utils";
import { UserContentFacade } from "./UserContent";

export class UserContentRouter extends Router {
  facade = new UserContentFacade();

  @Get("/me", [authenticate()])
  async me(ctx: Context) {
    return await this.facade.getOrCreate(ctx.state.user);
  }

  @Patch("/", [koaBody(), checkResourceId(), authenticate(["admin", "own"])])
  async update(ctx: Context) {
    const entity = ctx.request.body;
    return await this.facade.update(entity);
  }

  @Get("/share/:id/:key")
  async shareData(ctx: Context) {
    const { id, key } = ctx.params;
    const data = await this.facade.getById(id);
    if (!data || (data && !(data as any)[key])) {
      ctx.status = 404;
      return null;
    }
    return (data as any)[key];
  }
}
