import { BaseUserContentListItem, UserContent } from "mel-types";
import mongoose, { Document, Schema } from "mongoose";
import { BaseResourceFacade } from "../utils";

export type UserContentDocument = UserContent & Document;

const UserContentListItemSchema = new Schema<BaseUserContentListItem>({
  id: { type: String, required: true },
  _wrongVariantId: { type: Boolean, default: false },
});

const UserContentSchema = new Schema<UserContent>({
  closet: { type: [UserContentListItemSchema], default: [] },
  wishlist: { type: [UserContentListItemSchema], default: [] },
  coordinations: {
    type: [
      {
        id: { type: String },
        title: { type: String },
        theme: { type: String, default: null },
        event: { type: String, default: null },
        place: { type: String, default: null },
        date: { type: String },
        fields: { type: [Schema.Types.Mixed], default: [] },
      },
    ],
    default: [],
  },
  levelUpQuiz: { type: [], default: [] },
  user: { type: String, required: true },
  modified: { type: Number, required: true },
});

export const UserContentModel = mongoose.model(
  "UserContent",
  UserContentSchema
);

export class UserContentFacade extends BaseResourceFacade<
  UserContent,
  typeof UserContentModel,
  UserContentDocument
> {
  model = UserContentModel;

  async getOrCreate(user: string) {
    const existing = await this.model.findOne({ user });
    if (existing) {
      if (existing.levelUpQuiz === undefined) {
        existing.levelUpQuiz = [];
        await existing.save();
      }
      return existing;
    }

    return await this.model.create({ user, modified: Date.now() });
  }

  async stats(): Promise<any> {
    return this.model
      .aggregate([
        {
          $facet: {
            count_users: [{ $count: "total" }],
            count_coordi: [{ $unwind: "$coordinations" }, { $count: "total" }],
            count_closet: [{ $unwind: "$closet" }, { $count: "total" }],
            count_wishlist: [{ $unwind: "$wishlist" }, { $count: "total" }],
          },
        },
      ])
      .exec();
  }
}
