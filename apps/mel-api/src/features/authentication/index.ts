import { Context, Middleware, Next } from "koa";
import { DecodedIdToken, auth } from "mel-cloud-admin";
import { Roles } from "mel-types";

type UserClaims = DecodedIdToken & {
  custom_claims: { roles: Roles[] };
};

export function authenticate(roles?: Roles[]): Middleware {
  return async (ctx: Context, next: Next) => {
    const authorization = ctx.request.headers.authorization;
    if (!authorization) {
      ctx.status = 401;
    } else {
      try {
        const token = authorization.split(" ")[1];
        const decodedToken = await auth.verifyIdToken(token, true);
        const user = decodedToken as UserClaims;
        ctx.state.user = user;
        const userRoles = user.custom_claims.roles;
        if (roles) {
          const rolesMatch = roles.some((role) => userRoles?.includes(role));
          const ownMatch =
            roles.includes("own") &&
            (ctx.request?.body?.owner === user.sub ||
              ctx.request?.body?.user === user.sub);
          if (!rolesMatch || !ownMatch) {
            ctx.status = 403;
            return;
          }
        }
        await next();
      } catch (error) {
        ctx.status = 401;
      }
    }
  };
}
