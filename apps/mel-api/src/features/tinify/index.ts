import tinify from "tinify";
import { ENV } from "../env";

tinify.key = process.env[ENV.TINIFY_KEY] as string;

export default tinify;
