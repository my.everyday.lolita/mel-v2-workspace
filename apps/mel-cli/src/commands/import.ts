import { program } from "commander";
import inquirer from "inquirer";
import { firestore } from "mel-cloud-admin";
import { access, constants, readFile } from "node:fs/promises";

const transformers = {
  brands: (data: any): any => ({
    name: data.name,
    shortname: data.shortname,
    shop: data.shop,
  }),
  colors: (data: any): any => ({
    name: data.name,
    hex: data.hex,
  }),
  categories: (data: any): any => ({
    name: data.name,
    shortname: data.shortname,
    children: data.children,
  }),
  features: (data: any): any => ({
    name: data.name,
    categories: data.categories,
  }),
  items: (data: any): any => ({
    owner: data.owner,
    created: data.created,
    updated: data.modified,
    substyles: data.substyles,
    keywords: data.keywords,
    variants:
      data.variants
        .filter((v: any) => Array.isArray(v.colors) && v.colors.length > 0)
        ?.map((v: any) => ({
          colors: v.colors.map((c: any) => ({
            id: c._id,
            name: c.name,
            hex: c.hex,
          })),
          photos: v.photos,
        })) ?? [],
    features:
      data.features?.map((f: any) => ({
        id: f._id,
        name: f.name,
        categories: f.categories,
      })) ?? [],
    collection: data.collectionn,
    year: data.year,
    japanese: data.japanese,
    measurments: data.measurments,
    estimatedPrice: data.estimatedPrice,
    brand: {
      id: data.brand._id,
      name: data.brand.name,
      shortname: data.brand.shortname,
      shop: data.brand.shop,
    },
    category: data.category
      ? {
          name: data.category.name,
          shortname: data.category.shortname,
          disabled: data.category.disabled,
          lvl: data.category._lvlClass?.split("-")[1] ?? 0,
          parent: data.category.parent
            ? {
                id: data.category.parent._id ?? undefined,
                name: data.category.parent.name,
                parent: data.category.parent.parent
                  ? {
                      id: data.category.parent.parent._id ?? undefined,
                      name: data.category.parent.parent.name,
                    }
                  : undefined,
              }
            : undefined,
        }
      : null,
  }),
};

program
  .command("import")
  .description("Import data from json file (DEPRECATED)")
  .action(async () => {
    const answers = await inquirer.prompt([
      {
        type: "list",
        name: "type",
        message: "What do you want to import?",
        choices: ["Brands", "Colors", "Categories", "Features", "Items"],
        filter: (value: string) => value.toLowerCase(),
      },
      {
        type: "input",
        name: "path",
        message: "Enter a file path to the data",
        validate: async (input: string) => {
          if (!input) {
            return "Please enter a file path";
          }
          try {
            await access(input, constants.R_OK);
          } catch (error) {
            return error;
          }
          return true;
        },
      },
    ]);
    if (answers.path) {
      const file = await readFile(answers.path, "utf-8");
      const transformer =
        transformers[answers.type as keyof typeof transformers];
      if (!transformer) {
        console.error(`No transformer found for the type "${answers.type}"`);
        return;
      }
      const dataToImport: any[] = JSON.parse(file);
      if (Array.isArray(dataToImport)) {
        try {
          firestore.settings({ ignoreUndefinedProperties: true });
          const batch = firestore.batch();
          dataToImport.forEach((data) => {
            firestore
              .collection(answers.type)
              .doc(data._id)
              .set(transformer(data));
          });
          await batch.commit();
        } catch (error) {
          console.error(error);
        }
      } else {
        console.error("Wrong data type, it should be an array");
      }
    }
  });
