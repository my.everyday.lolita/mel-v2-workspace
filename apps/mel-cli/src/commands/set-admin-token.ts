import { program } from "commander";
import inquirer from "inquirer";
import { auth, UserRecord } from "mel-cloud-admin";

const valideEmailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

program
  .command("set-admin-token")
  .description(
    "Set admin privileges to a user (DEPRECATED, use manage-roles instead)"
  )
  .argument("[emailOrUid]", "the user email", undefined)
  .action(async (emailOrUid) => {
    let value = emailOrUid;
    if (!value) {
      const answer = await inquirer.prompt([
        {
          type: "input",
          name: "emailOrUid",
          message: "Enter a user email or a UID",
        },
      ]);
      value = answer.emailOrUid;
    }
    if (value) {
      const isEmail = valideEmailRegex.test(value);
      let user: UserRecord;
      try {
        if (isEmail) {
          user = await auth.getUserByEmail(value);
        } else {
          user = await auth.getUser(value);
        }
        await auth.setCustomUserClaims(user.uid, {
          admin: true,
        });
        console.log(`Admin token set for ${value}`);
      } catch (error) {
        console.error(error);
      }
    }
  });
