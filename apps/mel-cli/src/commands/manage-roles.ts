import { program } from "commander";
import inquirer from "inquirer";
import { UserRecord, auth } from "mel-cloud-admin";
import { roles } from "mel-types";

const valideEmailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

program
  .command("manage-roles")
  .description("Manage user roles")
  .argument("[emailOrUid]", "the user email", undefined)
  .action(async (emailOrUid) => {
    let value = emailOrUid;
    if (!value) {
      const answer = await inquirer.prompt([
        {
          type: "input",
          name: "emailOrUid",
          message: "Enter a user email or a UID",
        },
      ]);
      value = answer.emailOrUid;
    }
    if (value) {
      const isEmail = valideEmailRegex.test(value);
      let user: UserRecord;
      try {
        if (isEmail) {
          user = await auth.getUserByEmail(value);
        } else {
          user = await auth.getUser(value);
        }
        const claims = user.customClaims || { roles: [] };
        const selectedRoles = await inquirer.prompt([
          {
            type: "checkbox",
            name: "roles",
            message: "Select roles",
            choices: roles,
            default: claims.roles,
          },
        ]);

        await auth.setCustomUserClaims(user.uid, {
          ...claims,
          roles: selectedRoles.roles ?? [],
        });
        console.log(`Roles set for ${value}`);
      } catch (error) {
        console.error(error);
      }
    }
  });
