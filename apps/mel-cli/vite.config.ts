import { defineConfig } from "vite";
import { VitePluginNode } from "vite-plugin-node";

export default defineConfig({
  // ...vite configures
  server: {
    // vite server configs, for details see [vite doc](https://vitejs.dev/config/#server-host)
    port: 3001,
  },
  plugins: [
    ...VitePluginNode({
      adapter: "koa",
      appPath: "./cli.ts",
      exportName: "mel-api",
      tsCompiler: "esbuild",
      swcOptions: {},
    }),
  ],
});
