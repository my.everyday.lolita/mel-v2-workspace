import Profile from "@features/auth/Profile";
import { createMediaQuery } from "@solid-primitives/media";
import { useWindowSize } from "@solid-primitives/resize-observer";
import { useRoutes } from "@solidjs/router";
import Lace from "@ui/Lace";
import { Component, Show, lazy } from "solid-js";
import { Motion, Presence } from "solid-motionone";
import { routes } from "./routing";

const LazySideNav = lazy(() => import("./ui/navigation/SideNav"));
const LazyBottomNav = lazy(() => import("./ui/navigation/BottomNav"));

const App: Component = () => {
  const Routes = useRoutes(routes);
  const displaySideNav = createMediaQuery("(min-width: 640px)");
  const size = useWindowSize();
  return (
    <>
      <Lace class="fixed z-20 top-0 left-0" width={size.width} />
      <Profile />
      <Presence>
        <Show
          when={displaySideNav()}
          fallback={
            <Motion.div
              animate={{
                transform: ["translateY(25%)", "translateY(0%)"],
                opacity: [0, 1],
              }}
              exit={{ transform: "translateY(25%)", opacity: 0 }}
              transition={{ duration: 0.4 }}
              class="fixed bottom-0 z-10 h-auto w-full"
            >
              <LazyBottomNav />
            </Motion.div>
          }
        >
          <Motion.div
            initial={{ transform: "translateX(-50%)", opacity: 0 }}
            animate={{
              transform: "translateX(0%)",
              opacity: 1,
            }}
            exit={{ transform: "translateX(-50%)", opacity: 0 }}
            transition={{ duration: 0.4 }}
            class="sidenav-wrapper fixed z-10 left-0 top-0 h-full"
          >
            <LazySideNav />
          </Motion.div>
        </Show>
      </Presence>
      <main>
        <Routes />
      </main>
    </>
  );
};

export default App;
