import {
  AuthProvider,
  FacebookAuthProvider,
  GoogleAuthProvider,
  User,
  onAuthStateChanged,
  signInWithRedirect,
  signOut,
} from "firebase/auth";
import { createStore, produce } from "solid-js/store";
import { auth } from "./app";

type AvailableProviders = "google" | "facebook";

const providers: Record<AvailableProviders, AuthProvider> = {
  google: new GoogleAuthProvider(),
  facebook: new FacebookAuthProvider(),
};

onAuthStateChanged(auth, (user) => {
  setAccountState(
    produce(async (s) => {
      s.loading = false;
      s.account = user ?? undefined;
      if (s.account) {
        s.claims = (await user!.getIdTokenResult(true)).claims as any;
      } else {
        s.claims = undefined;
      }
    })
  );
});

export interface AccountState {
  account?: User;
  loading: boolean;
  claims?: {
    admin: boolean;
  };
}

const [accountState, setAccountState] = createStore<AccountState>({
  account: auth.currentUser ?? undefined,
  loading: true,
});

export const useAccount = () => {
  return {
    accountState,
    signOut: () => signOut(auth),
    signIn: (provider: AvailableProviders = "google") => {
      signInWithRedirect(auth, providers[provider]);
    },
  };
};
