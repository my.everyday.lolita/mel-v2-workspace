import { createContext, FlowProps, useContext } from "solid-js";

const DEFAULT_HIERARCHY = 1;

const hierarchyContext = createContext<number>(DEFAULT_HIERARCHY);

export function useHierarchy() {
  return useContext(hierarchyContext);
}

export const Hierarchy = (props: FlowProps & { reset: boolean }) => {
  const hierarchy = useHierarchy();
  return (
    <hierarchyContext.Provider
      value={props.reset ? DEFAULT_HIERARCHY : hierarchy + 1}
    >
      {props.children}
    </hierarchyContext.Provider>
  );
};
