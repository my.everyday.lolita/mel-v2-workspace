import { ComponentProps } from "solid-js";
import { Dynamic } from "solid-js/web";
import { useHierarchy } from "./Hierachy";

export const Title = (props: ComponentProps<"h1">) => {
  const hierarchy = useHierarchy();
  const as = () => `h${hierarchy}`;
  return <Dynamic component={as()} {...props} />;
};
