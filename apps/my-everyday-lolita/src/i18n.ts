import { createStore, produce } from "solid-js/store";

async function loadLocale(locale: string) {
  let messages = {};
  try {
    const localeModule = await import(`./i18n/${locale}.json`);
    messages = localeModule.default;
  } catch (error) {
    try {
      const localeModule = await import(`./i18n/${locale.split("-")[0]}.json`);
      messages = localeModule.default;
    } catch (error) {
      // Fall back to English
    }
  }
  return messages;
}

const initialLocale = localStorage.getItem("locale") ?? navigator.language;

export const [localeState, setLocaleState] = createStore({
  locale: initialLocale,
  messages: {},
});

loadLocale(initialLocale).then((messages) => {
  setLocaleState("messages", messages);
});

export async function setLocale(locale: string) {
  const messages = await loadLocale(locale);
  setLocaleState(
    produce((state) => {
      state.messages = messages;
      state.locale = locale;
      localStorage.setItem("locale", locale);
    })
  );
}
