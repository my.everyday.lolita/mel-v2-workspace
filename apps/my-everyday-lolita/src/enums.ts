export enum RoutePath {
  MyCloset = "/my-closet",
  MyWishlist = "/my-wishlist",
  Search = "/search",
  MyCoordChecklist = "/my-coord-checklist",
  AboutUs = "/about-us",
  AboutTheProject = "/about-the-project",
  Home = "/",
  Tips = "/tips",
  Games = "/games",
}
