import { defineMessages } from "@cookbook/solid-intl";
import { lazy } from "solid-js";
import { RoutePath } from "./enums";
import { AppRouteDefinition } from "./types";

export const routeMessages = defineMessages({
  myCloset: {
    id: "nav.labels.my-closet",
    defaultMessage: "My closet",
  },
  myWishlist: {
    id: "nav.labels.my-wishlist",
    defaultMessage: "My wishlist",
  },
  search: {
    id: "nav.labels.search",
    defaultMessage: "Search & add items",
  },
  myCoordChecklist: {
    id: "nav.labels.my-coord-checklist",
    defaultMessage: "My coord checklist",
  },
  home: {
    id: "nav.labels.home",
    defaultMessage: "Home",
  },
  aboutUs: {
    id: "nav.labels.about-us",
    defaultMessage: "About us",
  },
  aboutTheProject: {
    id: "nav.labels.about-the-project",
    defaultMessage: "About the project",
  },
  tips: {
    id: "nav.labels.tips",
    defaultMessage: "Tips",
  },
  games: {
    id: "nav.labels.games",
    defaultMessage: "Games",
  },
});

export const mainRoutes: AppRouteDefinition[] = [
  {
    path: RoutePath.MyCloset,
    label: routeMessages.myCloset,
    weight: 3,
    component: lazy(() => import("./pages/MyCloset")),
  },
  {
    path: RoutePath.MyWishlist,
    label: routeMessages.myWishlist,
    weight: 4,
    component: lazy(() => import("./pages/MyWishlist")),
  },
  {
    path: RoutePath.Search,
    label: routeMessages.search,
    weight: 2,
    sideNav: true,
    bottomNav: false,
    component: lazy(() => import("./pages/Search")),
  },
  {
    path: RoutePath.MyCoordChecklist,
    label: routeMessages.myCoordChecklist,
    weight: 5,
    component: lazy(() => import("./pages/MyCoordChecklist")),
  },
  {
    path: RoutePath.Home,
    label: routeMessages.home,
    weight: 1,
    end: true,
    component: lazy(() => import("./pages/Home")),
  },
];

export const otherRoutes: AppRouteDefinition[] = [
  {
    path: RoutePath.AboutUs,
    label: routeMessages.aboutUs,
    weight: 3,
    component: lazy(() => import("./pages/AboutUs")),
  },
  {
    path: RoutePath.AboutTheProject,
    label: routeMessages.aboutTheProject,
    weight: 4,
    component: lazy(() => import("./pages/AboutTheProject")),
  },
  {
    path: RoutePath.Tips,
    label: routeMessages.tips,
    weight: 1,
    component: lazy(() => import("./pages/Tips")),
  },
  {
    path: RoutePath.Games,
    label: routeMessages.games,
    weight: 2,
    component: lazy(() => import("./pages/Games")),
  },
];

export const routes = [...otherRoutes, ...mainRoutes];
