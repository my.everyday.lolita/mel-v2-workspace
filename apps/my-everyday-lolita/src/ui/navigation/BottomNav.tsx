import { mainRoutes, otherRoutes } from "@app/routing";
import { MessageDescriptor, defineMessages } from "@cookbook/solid-intl";
import { A, useLocation } from "@solidjs/router";
import { animate } from "motion";
import {
  OverlayScrollbarsComponent,
  OverlayScrollbarsComponentRef,
} from "overlayscrollbars-solid";
import { Component, ComponentProps, For, createEffect } from "solid-js";
import RouteLabel from "./RouteLabel";

const bottomNavMessages = defineMessages({
  more: {
    id: "bottomNav.more",
    defaultMessage: "More",
  },
});

interface BottomNavItemProps extends ComponentProps<"li"> {
  label: MessageDescriptor;
}

const BottomNavItem: Component<BottomNavItemProps> = (
  props: BottomNavItemProps
) => {
  return (
    <span class="w-full flex flex-col items-center gap-2 pt-4 pb-2">
      <span class="relative w-9 h-9 rounded-full bg-primary-content"></span>
      <span class="font-merienda text-[10px] leading-4 text-center capitalize select-none w-full min-w-0 text-ellipsis whitespace-nowrap overflow-hidden">
        <RouteLabel label={props.label} />
      </span>
    </span>
  );
};

const BottomNav: Component = () => {
  let scrollContainer!: OverlayScrollbarsComponentRef;
  const location = useLocation();

  const mainMenuRoutes = mainRoutes.filter(
    (r) => r.bottomNav === undefined || r.bottomNav
  );
  mainMenuRoutes.sort((a, b) => a.weight - b.weight);
  const otherMenuRoutes = otherRoutes.filter(
    (r) => r.bottomNav === undefined || r.bottomNav
  );
  otherMenuRoutes.sort((a, b) => a.weight - b.weight);
  const routes = [...mainMenuRoutes, ...otherMenuRoutes];

  const scrollTo = (container: HTMLElement, pathname: string) => {
    const element = container.querySelector<HTMLLIElement>(
      `[data-path="${pathname}"]`
    );
    if (!element) return;
    const start = container.scrollLeft;
    const end = Math.max(
      0,
      element.offsetLeft - container.clientWidth / 2 + element.clientWidth / 2
    );
    animate(
      (x) => {
        container.scrollTo({
          left: start + (end - start) * x,
        });
      },
      { duration: 0.5 }
    );
  };

  createEffect(() => {
    const container = scrollContainer
      .getElement()
      .querySelector<HTMLElement>(".os-viewport");
    const pathname = location.pathname;
    if (container && pathname) {
      scrollTo(container, pathname);
    }
  });

  return (
    <section class="relative bg-primary-shadow w-full rounded-t-2xl overflow-hidden">
      <h1 class="sr-only">Navigation menu</h1>
      <OverlayScrollbarsComponent
        ref={(r) => (scrollContainer = r)}
        defer
        class="overflow-left overflow-right"
        style={{ width: `${window.innerWidth}px` }}
        options={{
          scrollbars: { visibility: "hidden" },
        }}
        events={{
          initialized: (e) => {
            scrollTo(e.elements().viewport, location.pathname);
          },
        }}
      >
        <ul class="px-4 w-fit flex gap-4">
          <For each={routes}>
            {(route) => (
              <li class="w-16" data-path={route.path}>
                <A
                  href={route.path}
                  end={route.end}
                  class="transition-colors"
                  inactiveClass="text-primary-content"
                  activeClass="text-black"
                >
                  <BottomNavItem label={route.label} />
                </A>
              </li>
            )}
          </For>
        </ul>
      </OverlayScrollbarsComponent>
    </section>
  );
};

export default BottomNav;
