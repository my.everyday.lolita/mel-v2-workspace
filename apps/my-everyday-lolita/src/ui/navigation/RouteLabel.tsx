import { MessageDescriptor, useIntl } from "@cookbook/solid-intl";
import { Component, ComponentProps } from "solid-js";

interface LabelProps extends ComponentProps<"span"> {
  label: MessageDescriptor;
}

const RouteLabel: Component<LabelProps> = (props: LabelProps) => {
  const intl = useIntl();
  return <>{intl.formatMessage(props.label)}</>;
};

export default RouteLabel;
