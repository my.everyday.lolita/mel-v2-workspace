import { Title } from "@semantic/Title";
import { useWindowSize } from "@solid-primitives/resize-observer";
import { Component, ComponentProps, JSX, Show } from "solid-js";
import Lace from "./Lace";

interface SliverHeaderProps extends ComponentProps<"div"> {
  pageTitle?: string;
  content?: JSX.Element;
}

const SliverHeader: Component<SliverHeaderProps> = (
  props: SliverHeaderProps
) => {
  const size = useWindowSize();
  return (
    <>
      <header class="bg-secondary-shadow flex items-center h-20 pl-8 pr-16 sticky top-0 z-[1]">
        <Show when={props.pageTitle} fallback={props.content}>
          <Title class="text-xl text-ellipsis whitespace-nowrap overflow-hidden relative top-0.5">
            {props.pageTitle}
          </Title>
        </Show>
      </header>
      <div class="relative bg-secondary-shadow">
        <div class="sticky top-20 h-20 bg-gradient-to-b from-25% from-secondary-shadow" />
        {props.children}
      </div>
      <Lace secondary class="sticky top-20" width={size.width} />
    </>
  );
};

export default SliverHeader;
