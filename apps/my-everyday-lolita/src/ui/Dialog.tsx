import {
  Component,
  JSX,
  Show,
  createEffect,
  createMemo,
  createSignal,
} from "solid-js";
import { Motion, Presence } from "solid-motionone";
import { twMerge } from "tailwind-merge";

interface DialogProps {
  closeOnClickOutside?: boolean;
  open?: boolean;
  modal?: boolean;
  children: (api: DialogApi) => JSX.Element;
  duration?: number;
  class?: string;
  backdropClass?: string;
  onClose?: () => void;
}

interface DialogApi {
  close: () => void;
}

const Dialog: Component<DialogProps> = (props: DialogProps) => {
  const [visible, setVisible] = createSignal(false);
  const [ref, setRef] = createSignal<HTMLDialogElement>();
  createEffect(() => {
    setVisible(props.open);
  });
  const open = () => {
    const dialog = ref();
    if (!dialog) return;
    if (props.modal) {
      dialog.showModal();
    } else {
      dialog.show();
    }
  };
  createEffect((prev?: boolean) => {
    const dialog = ref();
    const shouldBeVisible = dialog && visible();
    if (!prev && shouldBeVisible) {
      open();
    }
    return shouldBeVisible;
  });
  const api = createMemo<DialogApi>(() => ({
    close() {
      setVisible(false);
      props.onClose?.();
    },
  }));
  return (
    <Presence>
      <Show when={visible()}>
        <Motion.dialog
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: props.duration ?? 0.4 }}
          ref={(elt) => setRef(elt)}
          class={twMerge(
            "p-0 border-0 bg-transparent backdrop:bg-transparent",
            props.class
          )}
          onKeyDown={(e) => {
            if (e.key === "Escape") {
              e.preventDefault();
              setVisible(false);
              props.onClose?.();
            }
          }}
        >
          <div
            class={twMerge(
              "fixed inset-0 bg-black bg-opacity-25",
              props.backdropClass
            )}
            onClick={() => {
              setVisible(false);
              props.onClose?.();
            }}
          />
          <div class="relative">{props.children(api())}</div>
        </Motion.dialog>
      </Show>
    </Presence>
  );
};

export default Dialog;
