import { Component, ComponentProps } from "solid-js";
import { twMerge } from "tailwind-merge";

interface LaceProps extends ComponentProps<"svg"> {
  width: number;
  secondary?: boolean;
}

const Lace: Component<LaceProps> = (props: LaceProps) => {
  const fill = () =>
    props.secondary ? "url(#lace-pattern-secondary)" : "url(#lace-pattern)";
  return (
    <svg
      class={twMerge("w-full h-4", props.class)}
      viewBox={[0, 0, props.width, 45].join(" ")}
    >
      <rect x="-100%" width="300%" height="100%" fill={fill()} />
    </svg>
  );
};

export default Lace;
