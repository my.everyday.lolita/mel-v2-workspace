import { defineMessages, MessageDescriptor } from "@cookbook/solid-intl";
import { A } from "@solidjs/router";
import { animate, spring } from "motion";
import { Component, ComponentProps, createSignal, onMount } from "solid-js";
import { createStore, produce } from "solid-js/store";
import { RoutePath } from "../../enums";
import { routeMessages } from "../../routing";
import RouteLabel from "./RouteLabel";

const bottomNavMessages = defineMessages({
  more: {
    id: "bottomNav.more",
    defaultMessage: "More",
  },
});

interface BottomNavItemProps extends ComponentProps<"li"> {
  label: MessageDescriptor;
}

const BottomNavItem: Component<BottomNavItemProps> = (
  props: BottomNavItemProps
) => {
  return (
    <span class="w-full flex flex-col items-center gap-2 pt-4 pb-2">
      <span class="relative w-9 h-9 rounded-full bg-secondary-darker"></span>
      <span class="font-merienda text-[10px] leading-4 text-center capitalize select-none w-full min-w-0 text-ellipsis whitespace-nowrap overflow-hidden">
        <RouteLabel label={props.label} />
      </span>
    </span>
  );
};

const BottomNav: Component = () => {
  let containerRef!: HTMLElement;
  let edge1 = window.innerHeight - 84;
  let edge2 = window.innerHeight - 3 * 84 - 16;
  const [opened, setOpened] = createSignal(false);
  onMount(() => {
    const firstRowItems =
      containerRef.querySelector<HTMLElement>("ul:first-of-type");
    const firstRowItemsHeight = firstRowItems.offsetHeight;
    edge1 = containerRef.offsetHeight - firstRowItemsHeight;
    const hiddenItems = containerRef.querySelectorAll<HTMLElement>(
      "ul:not(:first-of-type)"
    );
    const hiddenItemsHeight = Array.from(hiddenItems)
      .map((item) => item.offsetHeight)
      .reduce((a, b) => a + b, 0);
    console.log({
      parentHeight: containerRef.offsetHeight,
      firstRowItemsHeight,
      hiddenItemsHeight,
    });
    edge2 = edge1 - hiddenItemsHeight - 16;
    containerRef.style.transform = `translateY(${edge1}px)`;
  });
  const toggleHandler = () => {
    setOpened((prev) => !prev);
    animate(
      containerRef,
      {
        translateY: [opened() ? edge1 : edge2, opened() ? edge2 : edge1],
      } as any,
      {
        easing: spring({ stiffness: 75, damping: 10 }),
      }
    );
  };
  const [panState, setPanState] = createStore<{
    start?: number;
    opened?: boolean;
    translation?: number;
  }>({ start: undefined, opened: false });

  const onTouchStart = (event: TouchEvent) => {
    event.stopPropagation();
    document.body.style.overscrollBehaviorY = "none";
    setPanState(
      produce((state) => {
        state.opened = opened();
        state.start = event.touches[0].clientY;
      })
    );
  };
  const onTouchMove = (event: TouchEvent) => {
    const translation =
      (panState.opened ? edge2 : edge1) -
      (panState.start - event.touches[0].clientY);
    containerRef.style.transform = `translateY(${translation}px)`;
    setPanState("translation", translation);
  };
  const onTouchEnd = () => {
    if (!panState.start || !panState.translation) return;
    const opened = panState.opened
      ? panState.translation < edge2 + 16
      : panState.translation < edge1 - 16;
    const targetEdge = opened ? edge2 : edge1;
    animate(
      containerRef,
      {
        translateY: [panState.translation, targetEdge],
      } as any,
      {
        easing: spring({ stiffness: 75, damping: 10 }),
      }
    );
    setOpened(opened);
    setPanState(
      produce((state) => {
        state.start = undefined;
        state.translation = undefined;
      })
    );
    document.body.style.overscrollBehaviorY = "auto";
  };
  const onTouchCancel = () => {
    if (panState.start && panState.translation) {
      animate(
        containerRef,
        { transform: `translateY(${panState.opened ? edge2 : edge1}px)` },
        { easing: spring() }
      );
    }
    document.body.style.overscrollBehaviorY = "auto";
  };

  return (
    <section
      ref={containerRef}
      onTouchStart={onTouchStart}
      onTouchMove={onTouchMove}
      onTouchEnd={onTouchEnd}
      onTouchCancel={onTouchCancel}
      class="h-full relative bg-primary-shadow w-full rounded-t-2xl px-2 pb-4 pointer-events-auto"
    >
      <h1 class="sr-only">Navigation menu</h1>
      <div class="absolute top-1 left-1/2 -translate-x-1/2 w-10 h-1 bg-neutral opacity-50 rounded-lg"></div>
      <ul class="grid grid-cols-4 gap-x-2">
        <li class="contents">
          <A href={RoutePath.Home}>
            <BottomNavItem label={routeMessages.home} />
          </A>
        </li>
        <li class="contents">
          <A href={RoutePath.MyCloset}>
            <BottomNavItem label={routeMessages.myCloset} />
          </A>
        </li>
        <li class="contents">
          <A href={RoutePath.MyWishlist}>
            <BottomNavItem label={routeMessages.myWishlist} />
          </A>
        </li>
        <li class="contents">
          <button onClick={() => toggleHandler()}>
            <BottomNavItem label={bottomNavMessages.more} />
          </button>
        </li>
      </ul>
      <ul class="grid grid-cols-4 gap-x-2">
        <li class="contents">
          <A href={RoutePath.MyCoordChecklist}>
            <BottomNavItem label={routeMessages.myCoordChecklist} />
          </A>
        </li>
        <li class="contents">
          <A href={RoutePath.Tips}>
            <BottomNavItem label={routeMessages.tips} />
          </A>
        </li>
        <li class="contents">
          <A href={RoutePath.Games}>
            <BottomNavItem label={routeMessages.games} />
          </A>
        </li>
      </ul>
      <ul class="grid grid-cols-4 gap-x-2">
        <li class="contents">
          <A href={RoutePath.AboutUs}>
            <BottomNavItem label={routeMessages.aboutUs} />
          </A>
        </li>
        <li class="contents">
          <A href={RoutePath.AboutTheProject}>
            <BottomNavItem label={routeMessages.aboutTheProject} />
          </A>
        </li>
      </ul>
    </section>
  );
};

export default BottomNav;
