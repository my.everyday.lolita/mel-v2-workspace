/* @refresh reload */
import "overlayscrollbars/overlayscrollbars.css";
import { render } from "solid-js/web";
import "./index.css";

import { IntlProvider } from "@cookbook/solid-intl";
import { Router } from "@solidjs/router";
import App from "./App";
import { localeState } from "./i18n";

const root = document.getElementById("root");

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
  throw new Error(
    "Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got mispelled?"
  );
}

render(
  () => (
    <IntlProvider messages={localeState.messages} locale={localeState.locale}>
      <Router>
        <App />
      </Router>
    </IntlProvider>
  ),
  root!
);
