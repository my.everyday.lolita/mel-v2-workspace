import { MessageDescriptor } from "@cookbook/solid-intl";
import { RouteDefinition } from "@solidjs/router";
import { RoutePath } from "./enums";

export type AppRouteDefinition = RouteDefinition<RoutePath> & {
  label: MessageDescriptor;
  weight: number;
  sideNav?: boolean;
  bottomNav?: boolean;
  end?: boolean;
};
