import { defineMessages, useIntl } from "@cookbook/solid-intl";
import { useAccount } from "@core/cloud";
import { Component, ComponentProps } from "solid-js";
import { twMerge } from "tailwind-merge";
import FacebookSignIn from "./FacebookSignIn";
import GoogleSignIn from "./GoogleSignIn";

interface OAuthProviderProps extends ComponentProps<any> {
  // add props here
}

const messages = defineMessages({
  signInWithGoogle: {
    id: "auth.signInWithGoogle",
    defaultMessage: "Sign in with Google",
  },
  signInWithFacebook: {
    id: "auth.signInWithFacebook",
    defaultMessage: "Sign in with Facebook",
  },
});

const OAuthProvider: Component<OAuthProviderProps> = (
  props: OAuthProviderProps
) => {
  const intl = useIntl();
  const { signIn } = useAccount();
  return (
    <section class={twMerge("flex flex-col gap-2", props.class)}>
      <GoogleSignIn
        class="flex gap-4 pl-2.5 pr-3 h-10 items-center rounded-sm drop-shadow"
        onClick={() => {
          signIn();
        }}
      >
        {intl.formatMessage(messages.signInWithGoogle)}
      </GoogleSignIn>
      <FacebookSignIn
        class="flex gap-4 pl-2.5 pr-3 h-10 items-center rounded-sm drop-shadow"
        onClick={() => {
          signIn("facebook");
        }}
      >
        {intl.formatMessage(messages.signInWithFacebook)}
      </FacebookSignIn>
    </section>
  );
};

export default OAuthProvider;
