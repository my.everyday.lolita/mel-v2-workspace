import Icon from "@ui/Icon";
import { Component, ComponentProps, splitProps } from "solid-js";
import { twMerge } from "tailwind-merge";

type GoogleSignInProps = ComponentProps<"button">;

const GoogleSignIn: Component<GoogleSignInProps> = (
  props: GoogleSignInProps
) => {
  const [classes, other] = splitProps(props, ["class"]);
  return (
    <button {...other} class={twMerge("facebook relative", classes.class)}>
      <Icon icon="facebook" class="relative z-10" />
      <span class="relative z-10 text-xs xs:text-sm sm:text-base">
        {props.children}
      </span>
    </button>
  );
};

export default GoogleSignIn;
