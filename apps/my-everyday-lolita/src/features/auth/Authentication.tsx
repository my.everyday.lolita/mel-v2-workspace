import { defineMessages, useIntl } from "@cookbook/solid-intl";
import Icon from "@ui/Icon";
import { Component, ComponentProps, splitProps } from "solid-js";
import { twMerge } from "tailwind-merge";
import OAuthProvider from "./OAuthProvider";

interface AuthenticationProps extends ComponentProps<"section"> {
  // add props here
}

const messages = defineMessages({
  header: {
    id: "auth.header",
    defaultMessage: "Authentication",
  },
  description: {
    id: "auth.description",
    defaultMessage: `
      Sign in to add your items and share it with others.<br/>
      You can also sync your data across all your devices.
    `,
  },
});

const Authentication: Component<AuthenticationProps> = (
  props: AuthenticationProps
) => {
  const [classes, others] = splitProps(props, ["class"]);
  const intl = useIntl();
  return (
    <section
      class={twMerge(
        "relative p-16 flex flex-col gap-4 rounded-lg",
        "border border-secondary",
        "overflow-hidden",
        classes.class
      )}
      {...others}
    >
      <Icon
        icon="cloud-sync"
        class="absolute -top-10 -right-5 rotate-12 w-40 h-40 opacity-20 xs:opacity-50 sm:opacity-90"
      />
      <h1 class="relative text-lg">{intl.formatMessage(messages.header)}</h1>
      <p
        class="relative"
        innerHTML={intl.formatMessage(messages.description)}
      />
      <div class="flex justify-center">
        <OAuthProvider />
      </div>
    </section>
  );
};

export default Authentication;
