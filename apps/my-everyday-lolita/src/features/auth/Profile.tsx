import { localeState, setLocale } from "@app/i18n";
import { useAccount } from "@core/cloud";
import Dialog from "@ui/Dialog";
import Icon from "@ui/Icon";
import { Component, Show, createSignal } from "solid-js";
import { Motion } from "solid-motionone";
import Authentication from "./Authentication";

const Profile: Component = () => {
  const { accountState, signOut } = useAccount();
  const [displayDialog, setDisplayDialog] = createSignal(false);
  return (
    <>
      <button
        class="rounded-full fixed z-50 top-7 max-sm:right-4 sm:left-4"
        onClick={() => {
          setDisplayDialog((prev) => !prev);
        }}
      >
        <Show
          when={!accountState.account}
          fallback={
            <span class="relative">
              <img
                src={accountState.account.photoURL}
                class="rounded-full w-8 h-8"
                referrerpolicy="no-referrer"
              />
              <Show when={accountState.claims?.admin}>
                <Icon
                  icon="admin"
                  class="w-4 h-4 absolute -right-1 -bottom-1"
                />
              </Show>
            </span>
          }
        >
          <Icon icon="profile" class="w-8 h-8" />
        </Show>
      </button>
      <Dialog
        modal
        open={displayDialog()}
        onClose={() => setDisplayDialog(false)}
        backdropClass="bg-opacity-20"
        class="top-6 left-3 m-0 sm:right-auto"
      >
        {(api) => (
          <Motion.div
            initial={{ opacity: 0, scale: 0 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0 }}
            transition={{ duration: 0.4 }}
            class="relative origin-[1rem_1rem]"
          >
            <button
              class="absolute z-10 top-2.5 left-2.5"
              onClick={() => api.close()}
            >
              <span class="sr-only">Close</span>
              <Icon icon="close" class="w-5 h-5" />
            </button>
            <Show
              when={!(!accountState.loading && !accountState.account)}
              fallback={
                <Authentication class="bg-white border-4 rounded-2xl p-2 pt-10 xs:p-8 xs:pt-16 sm:p-16 sm:pt-20" />
              }
            >
              <div class="px-2 py-10 w-full sm:w-80 bg-white rounded-2xl flex justify-center">
                <button
                  class="bg-primary text-primary-content px-8 py-4 rounded-lg"
                  onClick={() =>
                    setLocale(localeState.locale === "en" ? "fr" : "en")
                  }
                >
                  Toggle locale
                </button>
                <button onClick={() => signOut()}>Logout</button>
              </div>
            </Show>
          </Motion.div>
        )}
      </Dialog>
    </>
  );
};

export default Profile;
