import { Component, ComponentProps } from "solid-js";

interface MyWishlistProps extends ComponentProps<any> {
  // add props here
}

const MyWishlist: Component<MyWishlistProps> = (props: MyWishlistProps) => {
  return (
    <div>
      <h2>MyWishlist</h2>
    </div>
  );
};

export default MyWishlist;
