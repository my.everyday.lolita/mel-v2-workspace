import { Component, ComponentProps } from "solid-js";

interface AboutUsProps extends ComponentProps<any> {
  // add props here
}

const AboutUs: Component<AboutUsProps> = (props: AboutUsProps) => {
  return (
    <div>
      <h2>AboutUs</h2>
    </div>
  );
};

export default AboutUs;
