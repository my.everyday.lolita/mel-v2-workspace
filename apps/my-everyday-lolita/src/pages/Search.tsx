import { defineMessages, useIntl } from "@cookbook/solid-intl";
import SliverHeader from "@ui/SliverHeader";
import { Component } from "solid-js";

const searchMessages = defineMessages({
  title: {
    id: "search.title",
    defaultMessage: "Search & Add items",
  },
});

const Search: Component = () => {
  const intl = useIntl();
  return (
    <div class="text-white">
      <SliverHeader pageTitle={intl.formatMessage(searchMessages.title)}>
        <p class="pb-16 pt-20 px-8">Sliver Header content</p>
      </SliverHeader>
      <div class="h-[200vh]"></div>
    </div>
  );
};

export default Search;
