import { Component, ComponentProps } from "solid-js";

interface MyCoordChecklistProps extends ComponentProps<any> {
  // add props here
}

const MyCoordChecklist: Component<MyCoordChecklistProps> = (
  props: MyCoordChecklistProps
) => {
  return (
    <div>
      <h2>MyCoordChecklist</h2>
    </div>
  );
};

export default MyCoordChecklist;
