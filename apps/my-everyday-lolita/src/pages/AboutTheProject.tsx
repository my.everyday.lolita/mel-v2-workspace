import { Component, ComponentProps } from "solid-js";

interface AboutTheProjectProps extends ComponentProps<any> {
  // add props here
}

const AboutTheProject: Component<AboutTheProjectProps> = (
  props: AboutTheProjectProps
) => {
  return (
    <div>
      <h2>AboutTheProject</h2>
    </div>
  );
};

export default AboutTheProject;
