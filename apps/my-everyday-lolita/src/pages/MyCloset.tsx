import { Component, ComponentProps } from "solid-js";

interface MyClosetProps extends ComponentProps<any> {
  // add props here
}

const MyCloset: Component<MyClosetProps> = (props: MyClosetProps) => {
  return (
    <div>
      <h2>MyCloset</h2>
    </div>
  );
};

export default MyCloset;
