/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,css,md,mdx,html,json,scss}",
  ],
  darkMode: "class",
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      white: "#fff",
      black: "#000",
      neutral: {
        DEFAULT: "rgb(var(--neutral) / <alpha-value>)",
      },
      primary: {
        DEFAULT: "rgb(var(--primary) / <alpha-value>)",
        shadow: "rgb(var(--primary-shadow) / <alpha-value>)",
        darker: "rgb(var(--primary-darker) / <alpha-value>)",
        content: "rgb(var(--primary-content) / <alpha-value>)",
      },
      secondary: {
        DEFAULT: "rgb(var(--secondary) / <alpha-value>)",
        shadow: "rgb(var(--secondary-shadow) / <alpha-value>)",
        darker: "rgb(var(--secondary-darker) / <alpha-value>)",
        content: "rgb(var(--secondary-content) / <alpha-value>)",
      },
    },
    fontFamily: {
      merienda: ["Merienda", "cursive"],
      mali: ["Mali", "cursive"],
    },
    extend: {
      screens: {
        xs: "320px",
      }
    },
  },
  plugins: [],
};
