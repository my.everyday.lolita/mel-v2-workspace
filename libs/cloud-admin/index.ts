import admin from "firebase-admin";
import { getFirestore } from "firebase-admin/firestore";
import serviceAccount from "./adminsdk-key.json";

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as admin.ServiceAccount),
  databaseURL: process.env.FIREBASE_URL,
});

export const database = admin.database();

export const firestore = getFirestore();

export const auth = admin.auth();

export type { DecodedIdToken } from "firebase-admin/lib/auth/token-verifier";
export type { UserRecord } from "firebase-admin/lib/auth/user-record";
