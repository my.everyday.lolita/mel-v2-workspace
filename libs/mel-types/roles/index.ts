export type Roles =
  | "admin"
  | "create-brand"
  | "edit-brand"
  | "create-category"
  | "edit-category"
  | "create-color"
  | "edit-color"
  | "create-feature"
  | "edit-feature"
  | "create-item"
  | "own" // specific role to allow users to edit their own content only
  | "dashboard-access";

export const roles: Roles[] = [
  "admin",
  "create-brand",
  "edit-brand",
  "create-category",
  "edit-category",
  "create-color",
  "edit-color",
  "create-feature",
  "edit-feature",
  "create-item",
  "dashboard-access",
];
