export type FlattenKeys<
  T extends Record<string, unknown>,
  K = keyof T
> = K extends string
  ? T[K] extends Record<string, unknown>
    ? `${K}.${FlattenKeys<T[K]>}`
    : `${K}`
  : never;
