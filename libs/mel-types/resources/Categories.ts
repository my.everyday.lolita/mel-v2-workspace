export interface Category {
  name: string;
  shortname: string;
  children: Category[];
}
