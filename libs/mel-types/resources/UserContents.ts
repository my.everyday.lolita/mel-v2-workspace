/**
 * The variant ID is a string constructed from the item ID and color IDs,
 * following this format: `${ItemId}:${ColorId1},${ColorId2}`.
 *
 * Color IDs were utilized as the primary key for an item variant (refer to `ItemVariant` type in `./Items.ts`).
 */
export type VariantId = `${string}:${string}`;

export type BaseUserContentListItem = {
  id: VariantId;
  _wrongVariantId?: boolean;
};

export type ClosetItem = BaseUserContentListItem & {
  wantToSell: boolean;
};

export type WishlistItem = BaseUserContentListItem & {
  dreamDress?: boolean;
};

export type UserContent = {
  closet: ClosetItem[];
  wishlist: WishlistItem[];
  coordinations: any[];
  levelUpQuiz: any[];
  user: string;
  modified: number;
};
