import { Category } from "./Categories";

export interface Feature {
  name: string;
  categories: Category[];
}
