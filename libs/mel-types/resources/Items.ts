import { FlattenKeys } from "../helpers";
import { Brand } from "./Brands";
import { Category } from "./Categories";
import { Color } from "./Colors";
import { Feature } from "./Features";

export type ItemVariant = {
  colors: Color[];
  photos: string[];
};

export type Item = {
  owner: string;
  created: string;
  updated: string;
  substyles: string[];
  keywords: string[];
  variants: ItemVariant[];
  features?: Feature[];
  collection: string;
  year: number;
  japanese: string;
  measurments: string;
  estimatedPrice: number;
  brand: Brand & { id: string };
  category?: Category;
};

export type ItemFieldPaths = FlattenKeys<Item>;
