export interface Brand {
  name: string;
  shortname: string;
  shop: string;
}
